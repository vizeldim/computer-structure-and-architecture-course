.cseg  
.include "m169def.inc"
.org 0x1000
.include "print.inc"
.org	0	;
	jmp	start
	
.org	0x100

retez:	.db	"NEBUDE-LI PRSET NEZMOKNEM", 0	

//Bez bonusu, podprogramem je cekaci smycka (pause)
start: 
	; Inicializace zadobniku
	ldi r16, 0xFF
	out SPL, r16
	ldi r16, 0x04
	out SPH, r16
 
 	; Inicializace displaye
	call init_disp
	
	; Adresa zacatku retezce
	ldi	r30, low(2*retez)	
	ldi	r31, high(2*retez)	
	
	; vyplneni mezerami
	ldi r19, ' '
	ldi r20, ' '
	ldi r21, ' '
	ldi r22, ' '
	ldi r23, ' '
	ldi r24, ' '
	ldi r25, ' '
	ldi r26, ' '
	ldi r27, ' '	
	ldi r28, ' '

	ldi r18, 6 ; <---- Pocet zobrazitelnych znaku ( na display ) --> 4 - 10
	mov r29, r18

	; posunuti vsech znaku
move:
	mov r19, r20
	mov r20, r21
	mov r21, r22
	mov r22, r23
	mov r23, r24
	mov r24, r25
	mov r25, r26
	mov r26, r27
	
	cpi r28, 0    ; nacteni noveho znaku, pokud neni konec retezce (0) , jinak se n-krat(n:=R18) nacte za novy znak ' '
	breq blank
	
	mov r27, r28
	lpm r28, Z+	
	rjmp showLeft
  blank:
	ldi r27, ' '
	dec r18   
	breq start
	
		
showLeft:
	//call pause ; <<<==== Cekaci Smycka
	
	; POSTUPNE ZOBRAZENI ZNAKU ( Zprava -> Doleva )
		mov r16, r28
		mov r17, r29
		inc r17       ; Zaciname od 2. pozice, proto posledni pozice = pocet zobrazitelnych znaku + 1
	  	cpi r28, 0
	  	brne notZero
	  	ldi r16, ' '
	  notZero:
		call show_char

		mov r16, r27	; Nahraj znak	
		dec r17			; Sniz pozici znaku
		call show_char	; Zobraz znak

		mov r16, r26
		dec r17
		call show_char

		mov r16, r25
		dec r17
		call show_char

	cpi r17, 2       ; Kontrola, jestli nedoslo k prekroceni pozice prvniho znaku na display
	breq skipToEnd

		mov r16, r24
		dec r17
		call show_char

	cpi r17, 2
	breq skipToEnd
	
		mov r16, r23
		dec r17
		call show_char

	cpi r17, 2
	breq skipToEnd

		mov r16, r22
		dec r17
		call show_char

	cpi r17, 2
	breq skipToEnd

		mov r16, r21
		dec r17
		call show_char

	cpi r17, 2
	breq skipToEnd

		mov r16, r20
		dec r17
		call show_char

	cpi r17, 2
	breq skipToEnd

		mov r16, r19
		dec r17
		call show_char
skipToEnd:
	rjmp move

pause: 
	; Zapis hodnoty do zasobniku
	push r16
	in r16, SREG
	push r16
	push r17
	push r18

	; Cekaci Smycka
	ldi  r16, 11 ; cca 1s
    ldi  r17, 11
    ldi  r18, 11
	wait: 
		dec  r18
	    brne wait
	    dec  r17
	    brne wait
	    dec  r16
	    brne wait

	; Vyber hodnoty ze zasobniku
	pop r18
	pop r17
	pop r16
	out SREG, r16
	pop r16
	ret
