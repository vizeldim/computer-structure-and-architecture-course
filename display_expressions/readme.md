# 1: Spočítejte následující výraz

R20 = (4 * R16 + 3 * R17 - R18) / 8

```
LDI R16, 5
LDI R17, 10
LDI R18, 58
... doplňte instrukce ..
```

- Operandy jsou 8-bitové, výsledek 8-bitový, veškeré mezivýsledky udržujte také 8-bitové.
- Operandy a výsledek jsou v doplňkovém kódu.
- Výraz by měl být spočítán správně. Pokud nelze výraz správně spočítat (s použitím 8-bitových registrů), program by měl toto indikovat (např. nastavením V flagu).

# 2: Napište program, který zobrazí na displeji zadané číslo v šestnáctkové soustavě

- Použijte knihovny pro práci s displejem z minulého cvičení.
- Použijte instrukce pro posuv a maskování, abyste dostali jednu šestnáctkovou číslici
- Použijte porovnání a sčítání, abyste z číslice vytvořili ASCII znak (kód ‘A’ je 65, kód ‘0’ je 48)

# 3a: Napište program, který sečte dvě 32 bitová čísla a zobrazí na displeji informaci o správnosti výsledku
- čísla považujte jednak za čísla nezáporná (bez znaménka) a jednak za čísla v doplňkovém kódu
- na displeji zobrazte buď hlášku overflow (OVER), pokud došlo k přetečení v doplňkovém kódu nebo carry (CARRY) v případě, že k přetečení nedošlo, ale vypadl nejvyšší bit (signalizace, že v případě interpretace čísel jako nezáporných se výsledek nevešel do registrů) nebo nulu (0) v případě, že se nenastavil ani příznak C, ani V.

# 3b: Napište program, který vypočítá výraz z Úkolu 1 vždy správně a zobrazí na displeji
- Zdrojové operandy budou stále 8-bitové (R16-R18), ale výsledek bude 16-bitový
- Toto 16-bitové číslo zobrazte na displeji (bez znaménka)
- Jediný možný nesprávný výsledek je způsobený chybou přesnosti. Toto indikujte na displeji (spolu se zaokrouhleným výsledkem)
